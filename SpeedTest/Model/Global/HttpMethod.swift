//
//  HttpMethod.swift
//  SpeedTest
//
//  Created by Jakub Marek on 20/05/2018.
//  Copyright © 2018 ubiquiti. All rights reserved.
//

/**
 HTTP methods
 - head
 - get
 - post
 - put
 - delete
 - options
 - trace
 - connect
 */
enum HttpMethod: EnumIdentifiable {

    case head
    case get
    case post
    case put
    case delete
    case options
    case trace
    case connect

    var value: String {
        return identifier.uppercased()
    }
}
