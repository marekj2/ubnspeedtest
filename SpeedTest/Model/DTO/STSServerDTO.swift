//
//  STSServerDTO.swift
//  SpeedTest
//
//  Created by Jakub Marek on 20/05/2018.
//  Copyright © 2018 ubiquiti. All rights reserved.
//

import CoreLocation
import Foundation

/**
 Speed testing server data transfer object.
 */
struct STSServerDTO: Codable {

    let url: String
    let latitude: CLLocationDegrees
    let longitude: CLLocationDegrees
    let provider: String?
    let city: String?
    let country: String?
    let countryCode: String?
    let speedMbps: Int?
}

extension STSServerDTO {

    /// STS server coordivates
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }

    /**
     Plain text initializator
     - Parameter textString: Columns are in order: `url`, `latitude`, `longitude`, `provider`, `city`, `country`, `countryCode`, `speedMbps`; separated by tab character.
     */
    init?(textString: String) {
        var items = textString.components(separatedBy: String.tab)
        guard let url = items.popFirst(),
            let latitudeString = items.popFirst(),
            let latitude = Double(latitudeString),
            let longitudeString = items.popFirst(),
            let longitude = Double(longitudeString) else {
                return nil
        }

        self.url = url
        self.latitude = latitude
        self.longitude = longitude
        self.provider = items.popFirst()
        self.city = items.popFirst()
        self.country = items.popFirst()
        self.countryCode = items.popFirst()
        if let speedMbpsString = items.popFirst() {
            self.speedMbps = Int(speedMbpsString)
        }
        else {
            self.speedMbps = nil
        }
    }
}
