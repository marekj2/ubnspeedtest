//
//  TokenDTO.swift
//  SpeedTest
//
//  Created by Jakub Marek on 20/05/2018.
//  Copyright © 2018 ubiquiti. All rights reserved.
//

import Foundation

/**
 Token object for autorizing network requests.
 */
struct TokenDTO: Codable {

    let token: String
    let ttl: UInt
}

extension TokenDTO {

    /**
     Plain text initializator
     - Parameter textString Columns are in order: `token`, `ttl`; separated by tab character.
     */
    init?(textString: String) {
        var items = textString.components(separatedBy: String.tab)
        guard let token = items.popFirst(),
            let ttlString = items.popFirst(),
            let ttl = UInt(ttlString) else {
                return nil
        }
        self.token = token
        self.ttl = ttl
    }
}
