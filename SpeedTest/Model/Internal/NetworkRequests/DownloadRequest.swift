//
//  DownloadRequest.swift
//  SpeedTest
//
//  Created by Jakub Marek on 20/05/2018.
//  Copyright © 2018 ubiquiti. All rights reserved.
//

/**
 Data holder create request for a downloading random file.
 */
struct DownloadRequest: HttpRequestProtocol {

    let baseUrlString: String
    let token: String?

    var method: HttpMethod {
        return .get
    }
    var path: String {
        return "/download"
    }
    var addressParams: [String: Any]? {
        return [
            "size": Configuration.DownloadSize,
        ]
    }
}
