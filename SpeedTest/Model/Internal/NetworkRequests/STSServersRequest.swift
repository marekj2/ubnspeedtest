//
//  STSServersRequest.swift
//  SpeedTest
//
//  Created by Jakub Marek on 20/05/2018.
//  Copyright © 2018 ubiquiti. All rights reserved.
//

import CoreLocation

/**
 Data holder create request for a STS servers receiving.
 */
struct STSServersRequest: HttpRequestProtocol {

    var baseUrlString: String {
        return Configuration.DSServerUrlString
    }
    var method: HttpMethod {
        return .get
    }
    var path: String {
        return "/api/v2/servers"
    }
    var addressParams: [String: Any]? {
        guard let coordinate = coordinate else {
            return nil
        }
        return [
            "latitude": coordinate.latitude,
            "longitude": coordinate.longitude,
        ]
    }
    let token: String?
    let coordinate: CLLocationCoordinate2D?
}
