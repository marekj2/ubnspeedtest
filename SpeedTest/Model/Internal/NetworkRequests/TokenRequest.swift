//
//  TokenRequest.swift
//  SpeedTest
//
//  Created by Jakub Marek on 20/05/2018.
//  Copyright © 2018 ubiquiti. All rights reserved.
//

/**
 Data holder create request for a token receiving.
 */
struct TokenRequest: HttpRequestProtocol {

    var baseUrlString: String {
        return Configuration.DSServerUrlString
    }
    var method: HttpMethod {
        return .post
    }
    var path: String {
        return "/api/v1/tokens"
    }
    var addressParams: [String: Any]? {
        return nil
    }
    var token: String? {
        return nil
    }
}
