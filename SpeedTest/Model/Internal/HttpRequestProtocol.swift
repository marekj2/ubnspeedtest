//
//  HttpRequestProtocol.swift
//  SpeedTest
//
//  Created by Jakub Marek on 20/05/2018.
//  Copyright © 2018 ubiquiti. All rights reserved.
//

/**
 Data holder object for creating network request.
 */
protocol HttpRequestProtocol {

    var baseUrlString: String { get }
    var method: HttpMethod { get }
    var path: String { get }
    var addressParams: [String: Any]? { get }
    var token: String? { get }
}

extension HttpRequestProtocol {

    var request: URLRequest? {
        guard let url = url else {
            return nil
        }
        var request = URLRequest(url: url)
        request.httpMethod = method.value
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        // WARNING: Doesn't return json at this case!
        // request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        // WORKAROUND: Using flow from http://speedtest.ubncloud.com/
        request.setValue("*/*", forHTTPHeaderField: "Accept")
        request.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData

        if let token = token {
            request.setValue(token, forHTTPHeaderField: "x-test-token")
        }
        return request
    }

    var baseUrl: URL? {
        return URL(string: baseUrlString)
    }

    var url: URL? {
        guard let url = URL(string: path, relativeTo: baseUrl) else {
            return nil
        }
        if let queryParams = addressParams, var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true) {
            urlComponents.queryItems = []
            for (queryName, queryValue) in queryParams {
                urlComponents.queryItems?.append(URLQueryItem(name: queryName, value: "\(queryValue)"))
            }
            return urlComponents.url
        }
        return url
    }

    var urlString: String {
        return url?.absoluteString ?? String.empty
    }
}
