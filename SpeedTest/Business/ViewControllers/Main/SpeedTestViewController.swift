//
//  SpeedTestViewController.swift
//  SpeedTest
//
//  Created by Jakub Marek on 20/05/2018.
//  Copyright © 2018 ubiquiti. All rights reserved.
//

import CoreLocation
import Foundation
import GBPing
import UIKit

final class SpeedTestViewController: UIViewController, ConsoleLoggable {

    // MARK: - Properties
    fileprivate var dispatchGroup: DispatchGroup?
    fileprivate var pingers: [GBPing] = [] {
        didSet {
            guard !pingers.isEmpty else {
                return
            }
            let group = DispatchGroup()
            self.dispatchGroup = group
            for pinger in pingers {
                group.enter()
                pinger.setup { (success, error) in
                    if success {
                        pinger.startPinging()
                    }
                    else {
                        if let localizedDescription = error?.localizedDescription {
                            self.logError(localizedDescription)
                        }
                        group.leave()
                    }
                }
            }
            // Performance issue: It is not necessary to wait for all...
            group.notify(queue: DispatchQueue.global()) {
                self.resolvePings()
            }
        }
    }
    fileprivate var currentSpeed: Int64? {
        didSet {
            DispatchQueue.main.async {
                guard let currentSpeed = self.currentSpeed else {
                    self.speedTestView.downloadLabel.text = .dash
                    return
                }
                self.speedTestView.downloadLabel.text = self.humanReadableSpeed(currentSpeed)
            }
        }
    }
    fileprivate var averageSpeed: Int64?
    fileprivate var timestamp: CFTimeInterval?
    fileprivate var timeoutTimer: Timer?
    fileprivate var currentTask: URLSessionTask?
    fileprivate var bestResult: (rtt: TimeInterval, pinger: GBPing)?
    private let apiService: APIServiceProtocol = APIService()
    private var servers: [STSServerDTO] = [] {
        didSet {
            pingers = servers.compactMap {
                let pinger = GBPing()
                pinger.host = $0.url.withoutProtocol
                pinger.delegate = self
                return pinger
            }
        }
    }
    private let speedTestView = SpeedTestView()

    // MARK: - Life-cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        view = speedTestView

        speedTestView.onStartClick = {
            self.start()
        }
        speedTestView.onStopClick = {
            self.stop()
        }
    }

    // MARK: - Actions
    fileprivate func start() {
        guard LocationManager.manager.checkLocationServices() else {
            showSettingsAlert()
            stop()
            return
        }
        timeoutTimer = Timer.scheduledTimer(
            timeInterval: Configuration.MaximumTimeForSpeedCheck,
            target: self,
            selector: #selector(stop),
            userInfo: nil,
            repeats: false
        )
        speedTestView.resetLabels()
        speedTestView.startButton.isHidden = true
        speedTestView.stopButton.isHidden = false
        bestResult = nil
        receiveToken { tokenObject in
            self.apiService.setToken(tokenObject.token)
            guard let coordinate = LocationManager.manager.location?.coordinate else {
                self.showAlert(MainSection.locationError.stringValue)
                self.stop()
                return
            }
            self.receiveServers(location: coordinate) { servers in
                self.servers = Array(servers.prefix(Configuration.NumberOfServersToPing))
            }
        }
    }

    @objc
    fileprivate func stop() {
        timeoutTimer?.invalidate()
        timeoutTimer = nil
        dispatchGroup = nil
        pingers.forEach {
            $0.stop()
        }
        currentTask?.cancel()
        DispatchQueue.main.async {
            if let averageSpeed = self.averageSpeed {
                self.speedTestView.downloadLabel.text = self.humanReadableSpeed(averageSpeed)
            }
            self.speedTestView.startButton.isHidden = false
            self.speedTestView.stopButton.isHidden = true
        }
    }

    // MARK: - Helpers
    fileprivate func showSettingsAlert() {
        let alert = UIAlertController(
            title: MainSection.alert.stringValue,
            message: MainSection.locationDescription.stringValue,
            preferredStyle: .alert
        )
        let okAction = UIAlertAction(
            title: MainSection.goToSettings.stringValue,
            style: .default) { _ in
                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(settingsUrl, completionHandler: nil)
                    }
                    else {
                        UIApplication.shared.openURL(settingsUrl)
                    }
                }
        }
        let cancelAction = UIAlertAction(
            title: MainSection.cancel.stringValue,
            style: .default,
            handler: nil
        )
        for action in [okAction, cancelAction] {
            alert.addAction(action)
        }
        present(alert, animated: true, completion: nil)
    }

    fileprivate func showAlert(_ message: String?) {
        let alert = UIAlertController(
            title: MainSection.alert.stringValue,
            message: message,
            preferredStyle: .alert
        )
        alert.addAction(
            UIAlertAction(
                title: MainSection.ok.stringValue,
                style: .default,
                handler: nil
            )
        )
        present(alert, animated: true, completion: nil)
    }

    // MARK: - Helpers
    fileprivate func receiveToken(_ callback: @escaping (TokenDTO) -> Void) {
        currentTask = apiService.getToken(
            success: { tokenObject in
                callback(tokenObject)
            },
            error: {
                self.stop()
            }
        )
        guard currentTask != nil else {
            stop()
            return
        }
    }

    fileprivate func receiveServers(location: CLLocationCoordinate2D, _ callback: @escaping ([STSServerDTO]) -> Void) {
        currentTask = apiService.getSTSServers(
            location: CLLocationCoordinate2D(latitude: 50.068_764, longitude: 14.391_847),
            success: { servers in
                callback(servers)
            },
            error: {
                self.stop()
            }
        )
        guard currentTask != nil else {
            stop()
            return
        }
    }

    fileprivate func resolvePings() {
        guard let bestResult = bestResult else {
            return
        }
        if let enumeratedItem = pingers.enumerated().first(where: { return $0.element == bestResult.pinger }),
            let server = servers[safe: enumeratedItem.offset] {
            DispatchQueue.main.async {
                self.speedTestView.nameLabel.text = server.city
                self.speedTestView.pingLabel.text = self.humanReadableResponse(bestResult.rtt)
            }
            self.downloadFile(server)
        }
        else {
            stop()
        }
    }

    fileprivate func downloadFile(_ server: STSServerDTO) {
        timestamp = CACurrentMediaTime()
        currentTask = apiService.downloadTask(baseUrlString: server.url, delegate: self)
        guard currentTask != nil else {
            stop()
            return
        }
    }

    /**
     Transforms speed in B/s to string value in human readable values like kB/s or MB/s.
     - Parameter speed: The speed in B/s.
     - Returns: The string value in human readable values like kB/s or MB/s.
     */
    fileprivate func humanReadableSpeed(_ speed: Int64) -> String {
        let stringValue = ByteCountFormatter().string(fromByteCount: speed)
        return "\(stringValue)/s"
    }

    /**
     Transforms `TimeInterval` to string value in ms.
     - Parameter interval: Time interval.
     - Returns: The string value in ms.
     */
    fileprivate func humanReadableResponse(_ interval: TimeInterval) -> String {
        return "\(Int(interval * 1000)) ms"
    }
}

// MARK: - <GBPingDelegate>
extension SpeedTestViewController: GBPingDelegate {

    func ping(_ pinger: GBPing!, didFailToSendPingWith summary: GBPingSummary!, error: Error!) {
        dispatchGroup?.leave()
        pinger.stop()
    }

    func ping(_ pinger: GBPing!, didFailWithError error: Error!) {
        pinger.stop()
    }

    func ping(_ pinger: GBPing!, didTimeoutWith summary: GBPingSummary!) {
        dispatchGroup?.leave()
        pinger.stop()
    }

    func ping(_ pinger: GBPing!, didReceiveReplyWith summary: GBPingSummary!) {
        if bestResult == nil || bestResult!.rtt < summary.rtt {
            bestResult = (summary.rtt, pinger)
        }
        pinger.stop()
        dispatchGroup?.leave()
    }

    func ping(_ pinger: GBPing!, didReceiveUnexpectedReplyWith summary: GBPingSummary!) {
        dispatchGroup?.leave()
        pinger.stop()
    }
}

// MARK: - <DownloadSpeedResultDelegate>
extension SpeedTestViewController: DownloadSpeedResultDelegate {

    func downloadSpeedProvider(_ provider: DownloadSpeedProvider, updateCurrentSpeed currentSpeed: Int64, averageSpeed: Int64) {
        self.currentSpeed = currentSpeed
        self.averageSpeed = averageSpeed
    }

    func downloadSpeedProvider(_ provider: DownloadSpeedProvider, didFinishWithAverageSpeed averageSpeed: Int64) {
        self.averageSpeed = averageSpeed
        stop()
    }

    func downloadSpeedProvider(_ provider: DownloadSpeedProvider, didFinishWithError error: Error?) {
        stop()
    }
}
