//
//  Loggable.swift
//  SpeedTest
//
//  Created by Jakub Marek on 20/05/2018.
//  Copyright © 2018 ubiquiti. All rights reserved.
//

import Foundation

public enum InfoLogLevel: Int, EnumIdentifiable, CustomStringConvertible {

    case all = 0
    case verbose = 1
    case info = 2
    case debug = 3
    case warning = 4
    case error = 5
    case none = 6

    public var description: String {
        return identifier.uppercased()
    }
}

public func >= (lhs: InfoLogLevel, rhs: InfoLogLevel) -> Bool {
    return lhs.rawValue >= rhs.rawValue
}

public protocol Loggable {

    var loggingDebugLevel: InfoLogLevel { get }
    var loggerIdentificationString: String { get }

    func logVerbose(_ message: String, functionName: String, fileName: String, lineNumber: Int)
    func logDebug(_ message: String, functionName: String, fileName: String, lineNumber: Int)
    func logInfo(_ message: String, functionName: String, fileName: String, lineNumber: Int)
    func logWarning(_ message: String, functionName: String, fileName: String, lineNumber: Int)
    func logError(_ message: String, functionName: String, fileName: String, lineNumber: Int)

    func log(_ message: String, logLevel: InfoLogLevel, functionName: String, fileName: String, lineNumber: Int)
    func logItem(item: String)
}

public extension Loggable {

    var loggerIdentificationString: String {
        return (Bundle.main.infoDictionary?["CFBundleName"] as? String) ?? String.empty
    }

    public func logVerbose(_ message: String, functionName: String = #function, fileName: String = #file, lineNumber: Int = #line) {
        log(message, logLevel: .verbose, functionName: functionName, fileName: fileName, lineNumber: lineNumber)
    }

    public func logDebug(_ message: String, functionName: String = #function, fileName: String = #file, lineNumber: Int = #line) {
        log(message, logLevel: .debug, functionName: functionName, fileName: fileName, lineNumber: lineNumber)
    }

    public func logInfo(_ message: String, functionName: String = #function, fileName: String = #file, lineNumber: Int = #line) {
        log(message, logLevel: .info, functionName: functionName, fileName: fileName, lineNumber: lineNumber)
    }

    public func logWarning(_ message: String, functionName: String = #function, fileName: String = #file, lineNumber: Int = #line) {
        log(message, logLevel: .warning, functionName: functionName, fileName: fileName, lineNumber: lineNumber)
    }

    public func logError(_ message: String, functionName: String = #function, fileName: String = #file, lineNumber: Int = #line) {
        log(message, logLevel: .error, functionName: functionName, fileName: fileName, lineNumber: lineNumber)
    }

    public func log(_ message: String, logLevel: InfoLogLevel = .debug, functionName: String = #function, fileName: String = #file, lineNumber: Int = #line) {
        if logLevel >= loggingDebugLevel {
            let logEntry = generateLogEntry(message, logLevel: logLevel, functionName: functionName, fileName: fileName, lineNumber: lineNumber)
            logItem(item: logEntry)
        }
    }

    fileprivate func generateLogEntry(_ message: String, logLevel: InfoLogLevel = .debug, functionName: String = #function, fileName: String = #file, lineNumber: Int = #line) -> String {
        let now = Date()
        let dateString = TimeFormats.dateTimeLog.stringFromDate(now)
        var threadString: String
        if Thread.isMainThread {
            threadString = "main"
        }
        else {
            if let threadName = Thread.current.name, !threadName.isEmpty {
                threadString = "\(threadName)"
            }
            else {
                threadString = String(format: "%p", Thread.current)
            }
        }
        let fileNameOnly = (fileName as NSString).lastPathComponent
        return "\(logLevel.description) \(dateString) \(loggerIdentificationString)[\(threadString):\(fileNameOnly):\(functionName):line \(lineNumber)]: \(message)"
    }
}
