//
//  ConsoleLaggable.swift
//  SpeedTest
//
//  Created by Jakub Marek on 20/05/2018.
//  Copyright © 2018 ubiquiti. All rights reserved.
//

protocol ConsoleLoggable: Loggable {

}

extension ConsoleLoggable {

    var loggingDebugLevel: InfoLogLevel {
        return Configuration.Logging.ConsoleLogLevel
    }

    func logItem(item: String) {
        DispatchQueue.main.async {
            print (item)
        }
    }
}
