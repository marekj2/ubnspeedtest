//
//  SectionTextProvider.swift
//  SpeedTest
//
//  Created by Jakub Marek on 20/05/2018.
//  Copyright © 2018 ubiquiti. All rights reserved.
//

import Foundation
import UIKit

protocol SectionTextProvider: TextProvider {

    var params: [String]? { get }
}

extension SectionTextProvider {

    var params: [String]? {
        return nil
    }
    var defaultStringValue: String {
        return message.replacePlaceHolders(params)
    }
    var stringValue: String {
        return defaultStringValue
    }
}
