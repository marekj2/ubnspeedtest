//
//  TextProvider.swift
//  SpeedTest
//
//  Created by Jakub Marek on 20/05/2018.
//  Copyright © 2018 ubiquiti. All rights reserved.
//

import Foundation

protocol TextProvider: EnumIdentifiable {

    var sectionName: String { get }
    var textIdentifier: String { get }
}

extension TextProvider {

    internal var defaultTextIdentifier: String {
        return itemName.firstCapitalizedString
    }
    internal var itemName: String {
        return identifier
    }
    internal var message: String {
        return NSLocalizedString("RS-\(sectionName)-\(textIdentifier)", tableName: sectionName, comment: String.empty)
    }
    var sectionName: String {
        return String(describing:
            Mirror(reflecting: self).subjectType
        ).replacingOccurrences(
            of: "Section$",
            with: String.empty,
            options: .regularExpression
        ).firstCapitalizedString
    }
}

extension TextProvider {

    var textIdentifier: String {
        return defaultTextIdentifier
    }
}

extension SectionTextProvider {

    var uppercasedStringValue: String {
        return stringValue.uppercased()
    }
}

extension TextProvider where Self: RawRepresentable, Self.RawValue == String {

    internal var itemName: String {
        return rawValue
    }
}
