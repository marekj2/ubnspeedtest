//
//  MainSection.swift
//  SpeedTest
//
//  Created by Jakub Marek on 20/05/2018.
//  Copyright © 2018 ubiquiti. All rights reserved.
//

public enum MainSection: SectionTextProvider {

    case alert
    case cancel
    case goToSettings
    case locationDescription
    case locationError
    case ok
    case start
    case stop

    var textIdentifier: String {
        switch self {
        case .ok:
            return "OK"
        default:
            return defaultTextIdentifier
        }
    }

}
