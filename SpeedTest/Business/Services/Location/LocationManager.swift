//
//  LocationManager.swift
//  SpeedTest
//
//  Created by Jakub Marek on 20/05/2018.
//  Copyright © 2018 ubiquiti. All rights reserved.
//

import CoreLocation
import Foundation

/// Wrapper around Core Location manager
final public class LocationManager: NSObject {

    // MARK: - Properties
    // MARK: Public Static Properties
    static let manager = LocationManager()

    // MARK: Public Properties
    var location: CLLocation? {
        return coreLocationManager.location
    }

    // MARK: Private properties
    private let coreLocationManager = CLLocationManager()

    // MARK: - Initialization
    fileprivate override init() {
        super.init()

        initializeLocationTracking()
    }

    /// Setup location manager
    fileprivate func initializeLocationTracking() {
        coreLocationManager.delegate = self
        coreLocationManager.requestWhenInUseAuthorization()
        coreLocationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
    }

    /**
     Check Location manager authorization status.
     - Returns true if location receiving is enabled; false otherwise
     */
    func checkLocationServices() -> Bool {
        if !CLLocationManager.locationServicesEnabled() {
            return false
        }

        let authorizationStatus = CLLocationManager.authorizationStatus()
        switch authorizationStatus {
        case .authorizedAlways:
            fallthrough
        case .authorizedWhenInUse:
            return true
        default:
            return false
        }
    }
}

// MARK: - <CLLocationManagerDelegate>
extension LocationManager: CLLocationManagerDelegate {

}
