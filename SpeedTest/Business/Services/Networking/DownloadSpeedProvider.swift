//
//  DownloadSpeedProvider.swift
//  SpeedTest
//
//  Created by Jakub Marek on 21/05/2018.
//  Copyright © 2018 ubiquiti. All rights reserved.
//

import Foundation

// MARK: - <DownloadSpeerResultDelegate>
protocol DownloadSpeedResultDelegate: class {

    func downloadSpeedProvider(_ provider: DownloadSpeedProvider, updateCurrentSpeed currentSpeed: Int64, averageSpeed: Int64)
    func downloadSpeedProvider(_ provider: DownloadSpeedProvider, didFinishWithAverageSpeed averageSpeed: Int64)
    func downloadSpeedProvider(_ provider: DownloadSpeedProvider, didFinishWithError error: Error?)
}

// MARK: - DownloadSpeedProvider
final class DownloadSpeedProvider: NSObject, ConsoleLoggable {

    weak var delegate: DownloadSpeedResultDelegate?

    fileprivate var startTimestamp: CFTimeInterval?
    fileprivate var lastTimestamp: CFTimeInterval?
    fileprivate var lastBytesWritten: Int64 = 0

    func initStartTime() {
        let timestamp = CACurrentMediaTime()
        startTimestamp = timestamp
        lastTimestamp = timestamp
        lastBytesWritten = 0
    }

    // MARK: - Helpers
    fileprivate func speed(_ bytesWritten: Int64, _ time: CFTimeInterval) -> Int64 {
        return Int64(Double(bytesWritten) / time)
    }
}

// MARK: - <URLSessionDownloadDelegate>
extension DownloadSpeedProvider: URLSessionDownloadDelegate {

    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        delegate?.downloadSpeedProvider(self, didFinishWithError: error)
    }

    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        let currentTimestamp = CACurrentMediaTime()
        guard let startTimestamp = startTimestamp, let lastTimestamp = lastTimestamp else {
            logError("Missing timestamp")
            return
        }

        delegate?.downloadSpeedProvider(
            self,
            updateCurrentSpeed: speed(totalBytesWritten - lastBytesWritten, currentTimestamp - lastTimestamp),
            averageSpeed: speed(totalBytesWritten, currentTimestamp - startTimestamp)
        )
        self.lastTimestamp = currentTimestamp
        self.lastBytesWritten = totalBytesWritten
    }

    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        guard let startTimestamp = startTimestamp else {
            logError("Missing timestamp")
            return
        }
        let currentTimestamp = CACurrentMediaTime()
        delegate?.downloadSpeedProvider(self, didFinishWithAverageSpeed: speed(Configuration.DownloadSize, currentTimestamp - startTimestamp))
    }
}
