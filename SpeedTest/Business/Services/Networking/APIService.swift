//
//  APIService.swift
//  SpeedTest
//
//  Created by Jakub Marek on 21/05/2018.
//  Copyright © 2018 ubiquiti. All rights reserved.
//

import CoreLocation
import Foundation

final class APIService: APIServiceProtocol, ConsoleLoggable {

    private var token: String?
    private var session = URLSession(configuration: .default)
    private var speedProvider = DownloadSpeedProvider()

    // MARK: - <APIServiceProtocol>
    @discardableResult
    func getToken(
        success: @escaping (TokenDTO) -> Void,
        error errorClosure: (() -> Void)? = nil
        ) -> URLSessionDataTask? {
        guard let request = TokenRequest().request else {
            fatalError("Could not create request.")
        }

        return executeDataTask(
            request,
            parse: { data, response in
                do {
                    if APIService.isPlainText(response),
                        let textValue = String(data: data, encoding: .utf8),
                        let token = TokenDTO(textString: textValue) {
                        success(token)
                    }
                    else {
                        let token = try JSONDecoder().decode(TokenDTO.self, from: data)
                        success(token)
                    }
                }
                catch {
                    errorClosure?()
                }
            },
            error: errorClosure
        )
    }

    @discardableResult
    func getSTSServers(
        location: CLLocationCoordinate2D? = nil,
        success: @escaping ([STSServerDTO]) -> Void,
        error errorClosure: (() -> Void)? = nil
    ) -> URLSessionDataTask? {
        guard let token = token else {
            logError("Token wasn't set!")
            errorClosure?()
            return nil
        }
        guard let request = STSServersRequest(token: token, coordinate: location).request else {
            fatalError("Could not create request.")
        }

        return executeDataTask(
            request,
            parse: { data, response in
                do {
                    if APIService.isPlainText(response),
                        let textValue = String(data: data, encoding: .utf8) {
                        let serverStrings = textValue.components(separatedBy: String.newLine)
                        let servers = serverStrings.compactMap {
                            return STSServerDTO(textString: $0)
                        }
                        success(servers)
                    }
                    else {
                        let servers = try JSONDecoder().decode([STSServerDTO].self, from: data)
                        success(servers)
                    }
                }
                catch {
                    errorClosure?()
                }
            },
            error: errorClosure
        )
    }

    func downloadTask(baseUrlString: String, delegate: DownloadSpeedResultDelegate) -> URLSessionDownloadTask? {
        guard let token = token else {
            logError("Token wasn't set!")
            return nil
        }
        guard let request = DownloadRequest (baseUrlString: baseUrlString, token: token).request else {
            fatalError("Could not create request.")
        }
        speedProvider.delegate = delegate
        session = URLSession(configuration: .default, delegate: speedProvider, delegateQueue: nil)
        let task = session.downloadTask(with: request)
        speedProvider.initStartTime()
        task.resume()
        return task
    }

    func setToken(_ token: String) {
        self.token = token
    }

    // MARK: - Helpers

    /**
     Executes data task based on given reqest and run parse closure on success and error on failure.
     - Parameters:
        - request: Given request,
        - parse Success closure.
        - error Errro closure.
     - Returns: Session data task.
     */
    fileprivate func executeDataTask(
        _ request: URLRequest,
        parse: @escaping (Data, URLResponse?) -> Void,
        error errorClosure: (() -> Void)? = nil
    ) -> URLSessionDataTask? {
        session = URLSession(configuration: .default)
        let task = session.dataTask(with: request) { (data, response, error) in
            guard error == nil, let data = data else {
                errorClosure?()
                return
            }
            // Parse
            parse(data, response)
        }

        task.resume()
        return task
    }

    /**
     Detects if given response is in plain text
     - Parameter response: Given response.
     - Returns: True is plain text is detected; false otherwise.
     */
    fileprivate static func isPlainText(_ response: URLResponse?) -> Bool {
        if let httpResponse = response as? HTTPURLResponse,
            let contentType = httpResponse.allHeaderFields["Content-Type"] as? String,
            contentType == "text/html; charset=utf-8" {
            return true
        }
        return false
    }
}
