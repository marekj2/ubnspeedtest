//
//  APIServiceProtocol.swift
//  SpeedTest
//
//  Created by Jakub Marek on 21/05/2018.
//  Copyright © 2018 ubiquiti. All rights reserved.
//

import CoreLocation
import Foundation

protocol APIServiceProtocol {

    /**
     Network request for receiving token object.
     - Parameters:
        - success: Success closure.
        - error: Error closure.
     - Returns: Session data task.
     */
    func getToken(success: @escaping (TokenDTO) -> Void, error errorClosure: (() -> Void)?) -> URLSessionDataTask?

    /**
     Network request for receiving STS servers.
     - Parameters:
        - location: Current location - optional,
        - success: Success closure.
        - error: Error closure.
     - Returns: Session data task.
     */
    func getSTSServers(
        location: CLLocationCoordinate2D?,
        success: @escaping ([STSServerDTO]) -> Void,
        error errorClosure: (() -> Void)?
    ) -> URLSessionDataTask?

    /**
     Network request for downloading a file.
     - Parameters:
        - baseUrlString: Address of chosen STS server,
        - delegate: Speed resuld delegate.
     - Returns: Session download task.
     */
    func downloadTask(
        baseUrlString: String,
        delegate: DownloadSpeedResultDelegate
    ) -> URLSessionDownloadTask?

    /**
     Network request for receiving STS servers.
     - Parameter token: New token value.
     */
    func setToken(_ token: String)
}
