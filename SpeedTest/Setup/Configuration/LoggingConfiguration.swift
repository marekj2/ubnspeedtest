//
//  LoggingConfiguration.swift
//  SpeedTest
//
//  Created by Jakub Marek on 20/05/2018.
//  Copyright © 2018 ubiquiti. All rights reserved.
//

extension Configuration {

    struct Logging {

        /// Logging level for console logs.
        static var ConsoleLogLevel: InfoLogLevel {
            return .all
        }
    }
}
