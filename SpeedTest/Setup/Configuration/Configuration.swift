//
//  Configuration.swift
//  SpeedTest
//
//  Created by Jakub Marek on 20/05/2018.
//  Copyright © 2018 ubiquiti. All rights reserved.
//

// MARK: - Generic Application Configuration.
/// Generic Application Configuration.
struct Configuration {

    // MARK: Computed Properties

    /// Base API URL (string format).
    static var DSServerUrlString: String {
        return "http://speedtest.ubncloud.com"
    }

    /// Required size of a downloaded file.
    static let DownloadSize: Int64 = 10_485_760

    /// Number of servers required to ping to.
    static let NumberOfServersToPing = 5

    /// Maimum time interval for testing the network connection speed.
    static let MaximumTimeForSpeedCheck: TimeInterval = 15
}
