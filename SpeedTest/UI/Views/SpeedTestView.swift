//
//  SpeedTestView.swift
//  SpeedTest
//
//  Created by Jakub Marek on 20/05/2018.
//  Copyright © 2018 ubiquiti. All rights reserved.
//

import Foundation

final class SpeedTestView: View {

    let uploadLabel = UILabel()
    let downloadLabel = UILabel()
    let pingLabel = UILabel()
    let nameLabel = UILabel()
    let startButton = UIButton()
    let stopButton = UIButton()

    var onStartClick: (() -> Void)?
    var onStopClick: (() -> Void)?

    fileprivate let middleContainerView = View()
    fileprivate let topCircleView = CircleView()
    fileprivate let midleCircleView = CircleView()
    fileprivate let bottomCircleView = CircleView()

    // MARK: - Actions
    @objc
    func start() {
        onStartClick?()
    }

    @objc
    func stop() {
        onStopClick?()
    }

    func resetLabels() {
        uploadLabel.text = .dash
        downloadLabel.text = .dash

        nameLabel.text = .dash
        pingLabel.text = .dash
    }

    // MARK: - <Initializable>
    override func initializeElements() {
        super.initializeElements()

        backgroundColor = .white

        resetLabels()

        startButton.addTarget(self, action: #selector(start), for: .touchUpInside)
        startButton.setTitle(MainSection.start.stringValue, for: .normal)
        startButton.backgroundColor = .green

        stopButton.addTarget(self, action: #selector(stop), for: .touchUpInside)
        stopButton.setTitle(MainSection.stop.stringValue, for: .normal)
        stopButton.backgroundColor = .red
        stopButton.isHidden = true

        for circleView in [topCircleView, midleCircleView, bottomCircleView] {
            circleView.clipsToBounds = true
            circleView.layer.borderColor = UIColor.black.cgColor
            circleView.layer.borderWidth = 3
            circleView.backgroundColor = .white
        }
    }

    override func addElementsToView() {
        super.addElementsToView()

        addSubviews(
            [
                midleCircleView,
                topCircleView,
                bottomCircleView,
                uploadLabel,
                downloadLabel,
                middleContainerView,
                stopButton,
                startButton,
            ]
        )
        middleContainerView.addSubviews(
            [
                pingLabel,
                nameLabel,
            ]
        )
    }

    override func setupConstraints() {
        super.setupConstraints()

        let allViews = [
            topCircleView,
            midleCircleView,
            bottomCircleView,
            uploadLabel,
            downloadLabel,
            middleContainerView,
            pingLabel,
            nameLabel,
            stopButton,
            startButton,
        ]

        for view in allViews {
            view.translatesAutoresizingMaskIntoConstraints = false
        }

        topCircleView.topAnchor.constraint(equalTo: topAnchor, constant: 20).isActive = true
        topCircleView.widthAnchor.constraint(equalTo: topCircleView.heightAnchor).isActive = true
        topCircleView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        topCircleView.widthAnchor.constraint(equalToConstant: 100).isActive = true

        midleCircleView.widthAnchor.constraint(equalTo: midleCircleView.heightAnchor).isActive = true
        midleCircleView.topAnchor.constraint(equalTo: topCircleView.centerYAnchor).isActive = true
        midleCircleView.bottomAnchor.constraint(equalTo: bottomCircleView.centerYAnchor).isActive = true
        midleCircleView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true

        bottomCircleView.bottomAnchor.constraint(equalTo: startButton.topAnchor, constant: -20).isActive = true
        bottomCircleView.widthAnchor.constraint(equalTo: bottomCircleView.heightAnchor).isActive = true
        bottomCircleView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        bottomCircleView.widthAnchor.constraint(equalToConstant: 100).isActive = true

        startButton.widthAnchor.constraint(equalTo: widthAnchor, constant: -20).isActive = true
        startButton.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        startButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        startButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20).isActive = true

        stopButton.widthAnchor.constraint(equalTo: startButton.widthAnchor).isActive = true
        stopButton.heightAnchor.constraint(equalTo: startButton.heightAnchor).isActive = true
        stopButton.centerXAnchor.constraint(equalTo: startButton.centerXAnchor).isActive = true
        stopButton.centerYAnchor.constraint(equalTo: startButton.centerYAnchor).isActive = true

        uploadLabel.centerYAnchor.constraint(equalTo: topCircleView.centerYAnchor).isActive = true
        uploadLabel.centerXAnchor.constraint(equalTo: topCircleView.centerXAnchor).isActive = true
        uploadLabel.widthAnchor.constraint(lessThanOrEqualTo: topCircleView.widthAnchor).isActive = true

        downloadLabel.centerYAnchor.constraint(equalTo: bottomCircleView.centerYAnchor).isActive = true
        downloadLabel.centerXAnchor.constraint(equalTo: bottomCircleView.centerXAnchor).isActive = true
        downloadLabel.widthAnchor.constraint(lessThanOrEqualTo: bottomCircleView.widthAnchor).isActive = true

        middleContainerView.centerYAnchor.constraint(equalTo: midleCircleView.centerYAnchor).isActive = true
        middleContainerView.centerXAnchor.constraint(equalTo: midleCircleView.centerXAnchor).isActive = true
        middleContainerView.widthAnchor.constraint(lessThanOrEqualTo: midleCircleView.widthAnchor).isActive = true

        pingLabel.topAnchor.constraint(equalTo: middleContainerView.topAnchor).isActive = true
        pingLabel.centerXAnchor.constraint(equalTo: middleContainerView.centerXAnchor).isActive = true
        pingLabel.widthAnchor.constraint(lessThanOrEqualTo: middleContainerView.widthAnchor).isActive = true

        nameLabel.bottomAnchor.constraint(equalTo: middleContainerView.bottomAnchor).isActive = true
        nameLabel.centerXAnchor.constraint(equalTo: middleContainerView.centerXAnchor).isActive = true
        nameLabel.widthAnchor.constraint(lessThanOrEqualTo: middleContainerView.widthAnchor).isActive = true
        nameLabel.topAnchor.constraint(equalTo: pingLabel.bottomAnchor).isActive = true
    }
}
