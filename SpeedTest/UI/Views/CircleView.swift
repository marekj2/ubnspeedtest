//
//  CircleView.swift
//  SpeedTest
//
//  Created by Jakub Marek on 20/05/2018.
//  Copyright © 2018 ubiquiti. All rights reserved.
//

import Foundation

final class CircleView: View {

    override var bounds: CGRect {
        didSet {
            layer.cornerRadius = bounds.width / 2.0
        }
    }
}
