//
//  View.swift
//  SpeedTest
//
//  Created by Jakub Marek on 20/05/2018.
//  Copyright © 2018 ubiquiti. All rights reserved.
//

import UIKit

class View: UIView, Initializable {

    required override init(frame: CGRect) {
        super.init(frame: frame)

        initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        initialize()
    }

    // MARK: - <Initializable>
    func addElementsToView() {}
    func initializeElements() {}
    func setupConstraints() {}
    func customInit() {}
}
