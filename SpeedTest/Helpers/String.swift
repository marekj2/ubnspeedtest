//
//  String.swift
//  SpeedTest
//
//  Created by Jakub Marek on 20/05/2018.
//  Copyright © 2018 ubiquiti. All rights reserved.
//

extension String {

    /// Empty string.
    static let empty: String = ""
    /// New line string.
    static let newLine: String = "\n"
    /// Single space string.
    static let space: String = " "
    /// A tab character.
    static let tab: String = "\t"
    /// A dash character.
    static let dash: String = "-"

    /// String with upper case first character.
    var capitalizedFirstLetter: String {
        let firstCharacter = prefix(1).capitalized
        let otherCharacters = String(dropFirst())
        return firstCharacter + otherCharacters
    }
    /// String with upper case first character of each word.
    var firstCapitalizedString: String {
        return components(separatedBy: CharacterSet.whitespaces).reduce(String.empty) { rest, component -> String in
            return rest + component.capitalizedFirstLetter
        }
    }
    /// String (URL String) without protocols (like ftp:// http:// etc.).
    var withoutProtocol: String {
        return replaceRegexMatches(pattern: protocolRegex)
    }

    fileprivate var protocolRegex: String {
        return "^\\w*:\\/\\/"
    }

    /**
     Replace regular expresion with given string
     - Parameters:
        - pattern: The regular expression.
        - replaceWith: The string to be replaced for.
     - Returns: The new replaced string.
     */
    func replaceRegexMatches(pattern: String, replaceWith: String = "") -> String {
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options.caseInsensitive)
            let range = NSRange(location: 0, length: count)
            return regex.stringByReplacingMatches(in: self, options: [], range: range, withTemplate: replaceWith)
        }
        catch {
            return self
        }
    }

    /**
     Returns a new string made by replacing ${n} placeholders by a given string parameters.
     - Parameter params: A string array containing the parameters to replace placeholders.
     - Returns: A new string made by replacing placeholders by a given parameters. If the receiver does not contain any placeholder, the origin string is returned.
     */
    func replacePlaceHolders(_ params: [String]?) -> String {
        var ret = self
        if let params = params {
            for (i, param) in params.enumerated() {
                ret = ret.replacingOccurrences(of: "${\(i)}", with: param)
            }
        }
        let range = ret.range(of: ret)
        ret = ret.replacingOccurrences(of: "\\$\\{\\d+\\}", with: String.empty, options: .regularExpression, range: range)

        return ret
    }
}
