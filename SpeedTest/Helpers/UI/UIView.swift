//
//  UIView.swift
//  SpeedTest
//
//  Created by Jakub Marek on 20/05/2018.
//  Copyright © 2018 ubiquiti. All rights reserved.
//

import Foundation

extension UIView {

    // MARK: - Static Properties
    fileprivate static let shimmeringAnimationKey = "shimmer"

    // MARK: - Computed Properties
    var shimmering: Bool {
        get {
            if let mask = layer.mask {
                if mask.animationKeys()?.contains(UIView.shimmeringAnimationKey) == true {
                    return true
                }
            }
            return false
        }
        set {
            guard shimmering != newValue else {
                return
            }
            if newValue {
                let animation = CABasicAnimation(keyPath: "locations")
                animation.fromValue = [0, 0.1, 0.2]
                animation.toValue   = [0.8, 0.9, 1]
                animation.duration = 1.5
                animation.repeatCount = Float.infinity

                let screenBounds = UIScreen.main.bounds
                let screenSize = max(screenBounds.width, screenBounds.height)
                let gradient = CAGradientLayer()
                gradient.colors = [
                    UIColor.black.cgColor,
                    UIColor.white.withAlphaComponent(0.01).cgColor,
                    UIColor.black.cgColor,
                ]
                gradient.frame = CGRect(x: -screenSize, y: 0, width: 3 * screenSize, height: screenSize)
                gradient.startPoint = CGPoint(x: 0, y: 0.5)
                gradient.endPoint = CGPoint(x: 1, y: 0.525)
                gradient.locations = [0.3, 0.5, 0.7]
                gradient.add(animation, forKey: UIView.shimmeringAnimationKey)
                layer.mask = gradient
            }
            else {
                layer.mask = nil
            }
        }
    }

    /**
     Adds an array of views to the end of the receiver’s list of subviews.
     - Parameter childViews: The array of views to be added.
     */
    func addSubviews(_ childViews: [UIView]) {
        childViews.forEach { addSubview($0) }
    }
}
