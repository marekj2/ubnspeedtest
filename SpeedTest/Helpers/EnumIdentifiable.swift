//
//  EnumIdentifiable.swift
//  SpeedTest
//
//  Created by Jakub Marek on 20/05/2018.
//  Copyright © 2018 ubiquiti. All rights reserved.
//

protocol EnumIdentifiable: Describable {

    var identifier: String { get }
}

extension EnumIdentifiable {

    var identifier: String {
        return typeName.components(separatedBy: CharacterSet.alphanumerics.inverted).first ?? .empty
    }
}
