//
//  Initializable.swift
//  SpeedTest
//
//  Created by Jakub Marek on 20/05/2018.
//  Copyright © 2018 ubiquiti. All rights reserved.
//

protocol Initializable {

    /**
     Initialization subroutine.
     */
    func initialize()

    /**
     Adds child elements as subviews.
     */
    func addElementsToView()

    /**
     Initializes all required elements.
     */
    func initializeElements()

    /**
     Setups required constraints.
     */
    func setupConstraints()

    /**
     Executes any custom initialization processes.
     */
    func customInit()
}

extension Initializable {

    // MARK: - Initialization

    /**
     Initialization subroutine.
     */
    func initialize() {
        defer {
            setupConstraints()
            customInit()
        }
        initializeElements()
        addElementsToView()
    }

    /**
     Adds child elements as subviews.
     */
    func addElementsToView() {}

    /**
     Initializes all required elements.
     */
    func initializeElements() {}

    /**
     Setups required constraints.
     */
    func setupConstraints() {}

    /**
     Executes any custom initialization processes.
     */
    func customInit() {}
}
