//
//  TimeFormats.swift
//  SpeedTest
//
//  Created by Jakub Marek on 20/05/2018.
//  Copyright © 2018 ubiquiti. All rights reserved.
//

import Foundation

/**
 Predefined timeformatters.
 - dateTimeLog
 */
public enum TimeFormats {

    /// yyyy-MM-dd HH:mm:ss.SSS
    case dateTimeLog

    fileprivate var formatString: String {
        switch self {
        case .dateTimeLog:
            return "yyyy-MM-dd HH:mm:ss.SSS"
        }
    }

    fileprivate var formatter: DateFormatter {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatString
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        return dateFormatter
    }

    /**
     Returns a date representation of a given string.
     - Parameter dateString: The string to parse.
     - Returns: A date representation of string. If dateFromString: can not parse the string, returns nil.
     */
    public func dateFromString(_ dateString: String?) -> Date? {
        guard let dateString = dateString else {
            return nil
        }

        return formatter.date(from: dateString)
    }

    /**
     Returns a string representation of a given date.
     - Parameter optionalDate: The date to format.
     - Returns: A string representation of date formatted. If date is nil, returns nil value.
     */
    public func stringFromDate(_ optionalDate: Date?) -> String? {
        guard let date = optionalDate else {
            return nil
        }

        return formatter.string(from: date)
    }

    /**
     Returns a string representation of a given date.
     - Parameter optionalDate: The date to format.
     - Returns: A string representation of date formatted.
     */
    public func stringFromDate(_ date: Date) -> String {
        return formatter.string(from: date)
    }
}
