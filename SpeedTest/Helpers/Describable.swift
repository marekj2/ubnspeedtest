//
//  Describable.swift
//  SpeedTest
//
//  Created by Jakub Marek on 20/05/2018.
//  Copyright © 2018 ubiquiti. All rights reserved.
//

protocol Describable {

    var typeName: String { get }
    static var typeName: String { get }
}

extension Describable {

    var typeName: String {
        return String(describing: self)
    }
    static var typeName: String {
        return String(describing: self)
    }
}

extension Describable where Self: NSObjectProtocol {

    var typeName: String {
        let type = Swift.type(of: self)
        return String(describing: type)
    }
}
